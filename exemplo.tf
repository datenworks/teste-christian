provider "aws" {
  region = "us-east-1"  
}

terraform {
  backend "s3" {
    bucket = "matheus-teste-123123123"
    key    = "exemplo/terraform.tfstate"
    region = "us-east-1"
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_lambda_function" "test_lambda" {
  filename      = "app.zip"
  function_name = "hello_world2"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "helloworld.hello_world"
  source_code_hash = filebase64sha256("app.zip")
  runtime = "python3.8"

  environment {
    variables = {
      foo = "bar"
    }
  }
}

resource "aws_api_gateway_rest_api" "APIexample" {
  name        = "ServerlessAppExample"
  description = "Terraform Serverless Application Example"
}

resource "aws_api_gateway_resource" "apiproxy" {
   rest_api_id = aws_api_gateway_rest_api.APIexample.id
   parent_id   = aws_api_gateway_rest_api.APIexample.root_resource_id
   path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "methodproxy" {
   rest_api_id   = aws_api_gateway_rest_api.APIexample.id
   resource_id   = aws_api_gateway_resource.apiproxy.id
   http_method   = "ANY"
   authorization = "NONE"
 }

resource "aws_api_gateway_integration" "integration_example" {
   rest_api_id = aws_api_gateway_rest_api.APIexample.id
   resource_id = aws_api_gateway_method.methodproxy.resource_id
   http_method = aws_api_gateway_method.methodproxy.http_method

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.test_lambda.invoke_arn
 }

resource "aws_api_gateway_deployment" "deploy_example" {
   depends_on = [
     aws_api_gateway_integration.integration_example,
   ]

   rest_api_id = aws_api_gateway_rest_api.APIexample.id
   stage_name  = "test"
}

resource "aws_lambda_permission" "apigw" {
   statement_id  = "AllowAPIGatewayInvoke"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.test_lambda.function_name
   principal     = "apigateway.amazonaws.com"

   # The "/*/*" portion grants access from any method on any resource
   # within the API Gateway REST API.
   source_arn = "${aws_api_gateway_rest_api.APIexample.execution_arn}/*/*"
}